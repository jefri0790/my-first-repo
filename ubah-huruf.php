<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php 
    

        function ubah_huruf($text)
        {
            $upperArr = range('A', 'Z') ;
            $text = strtoupper($text);
            $break = str_split($text);
            $newtext ="";
            for ($i=0; $i <count($break) ; $i++) { 
                $newtext .= $upperArr[array_search($break[$i],$upperArr)+1];
            }
            $newtext= strtolower($newtext);
            $newtext .= "<br>";
            return $newtext;
        
        }


        
        // TEST CASES
        echo ubah_huruf('wow'); // xpx
        echo ubah_huruf('developer'); // efwfmpqfs
        echo ubah_huruf('laravel'); // mbsbwfm
        echo ubah_huruf('keren'); // lfsfo
        echo ubah_huruf('semangat'); // tfnbohbu
    
    ?>
</body>
</html>