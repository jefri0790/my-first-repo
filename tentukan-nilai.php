<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php 
        function tentukan_nilai($nilai)
        {
            if ($nilai>=85 && $nilai <=100) {
                $hasil="Sangat Baik";
            } else if ($nilai>=70 && $nilai<85) {
                $hasil ="Baik";
            } else if ($nilai>=60 && $nilai<70) {
               $hasil="Cukup";
            }  else {
                $hasil="Kurang";
            }

            return $hasil;
            
        }
        //TEST CASES
            echo tentukan_nilai(98)."<Br>"; //Sangat Baik
            echo tentukan_nilai(76)."<Br>"; //Baik
            echo tentukan_nilai(67)."<Br>"; //Cukup
            echo tentukan_nilai(43)."<Br>"; //Kurang


    ?>
</body>
</html>